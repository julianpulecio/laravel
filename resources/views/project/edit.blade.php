<form method="post" action="{{ route('projects.update',$project->id) }}">
	{{ csrf_field() }}
	<ul>
		@forelse($errors->all() as $error)
			<li><p>{{$error}}</p></li>
		@empty
		@endforelse
	</ul>
	<input type="hidden" name="_method" value="PATCH">
	<input type="text" name="titulo" value="{{$project->titulo}}">
	<textarea name="descripcion" value={{old('descripcion')}}>
		{{$project->descripcion}}
	</textarea>
	<button type="submit">enviar</button>
</form>