<form method="post" action="{{ route('projects.store') }}">
	{{ csrf_field() }}
	<ul>
		@forelse($errors->all() as $error)
			<li><p>{{$error}}</p></li>
		@empty
		@endforelse
	</ul>
	<input type="text" name="titulo">
	<textarea name="descripcion" value={{old('descripcion')}}>
	</textarea>
	<button type="submit">enviar</button>
</form>