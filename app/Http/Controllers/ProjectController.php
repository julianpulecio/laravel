<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Requests\CreateProjectRequest;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::get()->toArray();
        return view('project.index',['projects'=>$projects]);
    }

    public function create()
    {
        return view('project.create');
    }

    public function store(CreateProjectRequest $request)
    {
        Project::create($request->validated());
        return redirect()->route('projects.index');
    }

    public function show($id)
    {
        $project = Project::find($id);
        return view('project.show',['project'=>$project]);
    }

    public function edit($id)
    {
        $project = Project::find($id);
        return view('project.edit',['project'=>$project]);
    }

    public function update(CreateProjectRequest $request, $id)
    {
        $project = Project::find($id);
        $project->update($request->validated());
        return redirect()->route('projects.index');
    }

    public function destroy($id)
    {
        //
    }
}
