<?php

Route::get('/projects','ProjectController@index')->name('projects.index');
Route::post('/projects/store','ProjectController@store')->name('projects.store');
Route::get('/projects/create','ProjectController@create')->name('projects.create');
Route::get('/projects/{id}','ProjectController@show')->name('projects.show');
Route::patch('/projects/{id}','ProjectController@update')->name('projects.update');
Route::get('/projects/edit/{id}','ProjectController@edit')->name('projects.edit');
Route::get('/', function () {
    return view('welcome');
});
